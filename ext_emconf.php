<?php

$EM_CONF['na_fluxdummy'] = [
    'title' => 'Naderios Flux-Dummy',
    'description' => 'A dummy extension, using flux and fluidpages',
    'category' => 'templates',
    'author' => 'Thomas Anders',
    'author_email' => 'me@naderio.de',
    'state' => 'beta',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '0.0.3',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-9.5.99',
            'flux' => '9.2.0-9.2.99'
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
