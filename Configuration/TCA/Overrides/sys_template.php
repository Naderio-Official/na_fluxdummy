<?php

defined('TYPO3_MODE') or die();

call_user_func(function () {
    /**************************************************************************
     * Create the full extension key including vendor name */
    $vendorName = 'Naderio';
    $extKey = 'na_fluxdummy';
    $fullExtensionKey = $vendorName . '.' . \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($extKey);

    /**************************************************************************
     * Add extensions TypoScript files to 'includes' selector in template module */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extKey, 'Configuration/TypoScript', 'Naderios Flux-Dummy');

    /**************************************************************************
     * Register Flux for content elements and page templates */
    \FluidTYPO3\Flux\Core::registerProviderExtensionKey($fullExtensionKey, 'Content');
    \FluidTYPO3\Flux\Core::registerProviderExtensionKey($fullExtensionKey, 'Page');
});


